var express = require('express');
var app = express();
var cors = require('cors');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');

var Country = require('./modules/Country.js');

var path = __dirname;
var port = 7000;

app.use(cors());
app.use(bodyParser.json());
app.use(express.static(path));

app.listen(port, function() {
	console.log('listening on port:' + port);
});


mongoose.connect('mongodb://admin:admin@ds127888.mlab.com:27888/second').then(function() {
	console.log('Connected to mongo')}, function(err) {
	console.log(err);
});

app.post('/addnew', function(req, res) {
	const itemData = req.body;
	const item = new Country(itemData);
	item.save(function(err,result) {
		if (err) {
			console.log(err);
		} else {
			console.log('New Country Added');
			res.send(result);
		}
	})
});

app.get('/all', function(req,res) {
	var response = res;
	Country.find({}).then(function(res) {
		response.send(res);
	});
});

app.post('/removeCountry', function(req,res) {
	var country = req.body;
	Country.remove({_id: country._id}, function(err) {
		if(err) {
			console.log(err);
		} else {
			res.send(country);
		}
	});
});

app.get('/fetchSpecific', function(req,res) {
	var country = req.query;
	var response = res;
	Country.find({_id: country.id}).then(function(res) {
		response.send(res[0]);
	})
});