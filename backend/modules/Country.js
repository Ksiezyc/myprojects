var mongoose = require('mongoose');

const countrySchema = new mongoose.Schema({
	name: String,
	capital: String,
	population: Number
});

module.exports = mongoose.model('Country', countrySchema);