'use strict';

angular.
  module('countryApp').
  config(['$stateProvider', '$locationProvider', '$urlRouterProvider',
    function config($stateProvider, $locationProvider, $urlRouterProvider) {

  	$urlRouterProvider.otherwise('/countries');

    $stateProvider
        .state('countries', {
          url: '/countries',
          template: '<country-list></country-list>'
        })
        .state('countrySpecificDetails', {
          url: '/countries/:id',
          template: '<country-page></country-page>'
      });
			$locationProvider.html5Mode(true);

    }
  ]);
