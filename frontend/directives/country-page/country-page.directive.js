'use strict';

angular.
module('countryApp')
		.directive('countryPage', function () {
			return {
				restrict: 'E',
				templateUrl: 'directives/country-page/country-page.template.html',
				scope: {},
				controllerAs: 'vm',
				controller:(['$http', '$resource', 'countryAPI', '$scope', '$stateParams', function countryListController($http, $resource, countryAPI, $scope, $stateParams) {
					var vm = this;
					vm.country = countryAPI.fetchSpecific({id: $stateParams.id});
				}])
			}
		});


