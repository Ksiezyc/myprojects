'use strict';

angular.
module('countryApp')
		.directive('countryHeader', function () {
			return {
				restrict: 'E',
				templateUrl: 'directives/header/header.template.html',
				scope: {}
			}
		});
