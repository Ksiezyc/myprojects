'use strict';

angular.
module('countryApp')
		.directive('countryDetails', function () {
		return {
			restrict: 'E',
			templateUrl: 'directives/country-details/country-details.template.html',
			scope: {
				countries: '=',
				order: '=',
				query: '='
			},
			controllerAs: 'vm',
			controller:(['$http', '$resource', 'countryAPI', '$scope', '$mdToast', function countryListController($http, $resource, countryAPI, $scope, $mdToast) {
				var vm = this;
				vm.removeCountry = removeCountry;


				function removeCountry(country) {
					countryAPI.removeCountry(country).$promise.then(function(){
						showSuccessToast();
						$scope.countries.splice($scope.countries.indexOf(country), 1);
					}).catch(function(err) {
						showFailureToast();
						console.log(err);
					});
				}

				function showSuccessToast() {
					$mdToast.show({
						hideDelay   : 1000,
						position    : 'top right',
						templateUrl : 'directives/country-details/toasts/toast-success.html'
					});
				}

				function showFailureToast() {
					$mdToast.show({
						hideDelay   : 1000,
						position    : 'top right',
						templateUrl : 'directives/country-details/toasts/toast-success.html'
					});
				}

			}])
		}
});


