'use strict';

angular.
module('countryApp')
		.directive('countryList', function () {
			return {
				restrict: 'E',
				templateUrl: 'directives/country-list/country-list.template.html',
				controllerAs: 'vm',
				controller:(['$http', '$resource', 'countryAPI', '$mdToast', function countryListController($http, $resource, countryAPI, $mdToast) {
					var vm = this;

					vm.name = '';
					vm.population = '';
					vm.capital = '';
					vm.orderProp = 'name';
					vm.query = '';

					vm.addCountry = addCountry;

					function addCountry() {
						countryAPI.addNew({name: vm.name, population: vm.population, capital: vm.capital})
								.$promise.then(function(result) {
							showSuccessToast();
							vm.name = '';
							vm.population = '';
							vm.capital = '';
							vm.countries.push(result);
						}).catch(function(err) {
							showFailureToast();
							console.log(err);
						});
					}

					function showSuccessToast() {
						$mdToast.show({
							hideDelay   : 1000,
							position    : 'top right',
							templateUrl : 'directives/country-list/toasts/toast-success.html'
						});
					}

					function showFailureToast() {
						$mdToast.show({
							hideDelay: 1000,
							position : 'top right',
							templateUrl : 'directives/country-list/toasts/toast-failure.html'
						})
					}

					vm.countries = countryAPI.fetchAll();

				}])
			}
		});






