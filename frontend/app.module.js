'use strict';

angular.module('countryApp', [
	'ngResource',
	'countryService',
	'ui.router',
	'ngMaterial'
]);