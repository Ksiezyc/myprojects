'use strict';

angular.
module('countryService', []).
factory('countryAPI', ['$resource',
	function($resource) {
		return $resource('http://localhost:7000/:first', {}, {
			fetchAll: {
				method: 'GET',
				params: {first: 'all'},
				isArray: 'true'
			},
			addNew: {
				method: 'POST',
				params: {first: 'addnew'}
			},
			removeCountry: {
				method: 'POST',
				params: {first: 'removeCountry'}
			},
			fetchSpecific: {
				method: 'GET',
				params: {first: 'fetchSpecific'}
			}
		});
	}
]);