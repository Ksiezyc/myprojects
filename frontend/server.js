var express = require('express');
var app = express();
var cors = require('cors');

var path = __dirname;
var port = 8000;

app.use(cors());
app.use(express.static(path));
app.get('*', function(req, res) {
  res.sendFile(path + '/index.html');
});
app.listen(port, function() {
  console.log('listening on port: ' + port);
});

